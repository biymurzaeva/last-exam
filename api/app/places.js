const express = require("express");
const path = require("path");
const multer = require("multer");
const {nanoid} = require("nanoid");
const config = require("../config");
const auth = require("../middleware/auth");
const Place = require("../models/Place");

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
	try {
		if (req.query.place) {
			const place = await Place.findById(req.query.place);
			return res.send(place);
		}

		const places = await Place.find();
		res.send(places);
	} catch (error) {
		res.status(500).send(error);
	}
});

router.post('/', auth,  upload.single('image'), async (req, res) => {
	try {
		if (req.body.agree === 'false') {
			return res.status(400).send({error: 'Check the box if you agree with the terms'});
		}

		const newPlace = {
			user: req.user._id,
			title: req.body.title,
			description: req.body.description,
		};

		if (req.file) newPlace.mainPhoto = 'uploads/' + req.file.filename;

		const place = new Place(newPlace);
		await place.save();
		res.send(place);
	} catch (error) {
		res.status(500).send(error);
	}
});

module.exports = router;