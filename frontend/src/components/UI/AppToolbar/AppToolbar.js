import React from 'react';
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
import {AppBar, Grid, Toolbar, Typography} from "@mui/material";
import {makeStyles} from "@mui/styles";
import UserMenu from "./Menu/UserMenu";
import AnonymousMenu from "./Menu/AnonymousMenu";

const useStyles = makeStyles(theme => ({
  mainLink: {
    color: "inherit",
    textDecoration: 'none',
    '$:hover': {
      color: 'inherit'
    }
  },
  staticToolbar: {
    marginBottom: theme.spacing(2)
  },
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
	},
}));

const AppToolbar = () => {
  const classes = useStyles();
  const user = useSelector(state => state.users.user);

  return (
    <>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
	        <Grid container justifyContent="space-between" alignItems="center">
		        <Grid item>
			        <Typography variant="h6">
				        <Link to="/" className={classes.mainLink}>WEB</Link>
			        </Typography>
		        </Grid>
		        <Grid item>
			        <Grid container justifyContent="flex-end" alignItems="center">
				        {user ? (
					        <Grid item>
						        <UserMenu user={user}/>
					        </Grid>
				        ) : (
					        <AnonymousMenu/>
				        )}
			        </Grid>
		        </Grid>
	        </Grid>
        </Toolbar>
      </AppBar>
      <Toolbar className={classes.staticToolbar}/>
    </>
  );
};

export default AppToolbar;