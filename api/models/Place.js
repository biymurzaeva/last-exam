const mongoose = require('mongoose');

const PlaceSchema = new mongoose.Schema({
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true,
	},
	title: {
		type: String,
		required: true,
	},
	description: {
		type: String,
		required: true,
	},
	mainPhoto: {
		type: String,
		required: true,
	},
	ratingFood: String,
	ratingInterior: String,
	ratingService: String,
});

const Place = mongoose.model('Place', PlaceSchema);
module.exports = Place;