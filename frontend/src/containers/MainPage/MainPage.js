import React, {useEffect} from 'react';
import {Container, Grid} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import PlaceCard from "../../components/PlaceCard/PlaceCard";
import {fetchPlaces} from "../../store/actions/placesActions";

const MainPage = () => {
	const dispatch = useDispatch();
	const places = useSelector(state => state.places.places);

	useEffect(() => {
		dispatch(fetchPlaces());
	}, [dispatch]);

	return (
		<Container maxWidth="md">
			<Grid container spacing={4}>
				{places && places.map(place => (
					<PlaceCard
						key={place._id}
						id={place._id}
						image={place.mainPhoto}
						title={place.title}
					/>
				))}
			</Grid>
		</Container>
	);
};

export default MainPage;