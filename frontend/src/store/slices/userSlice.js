const {createSlice} = require("@reduxjs/toolkit");

export const initialState = {
	user: {},
	registerLoading: false,
	registerError: null,
	loginLoading: false,
	loginError: null,
};

const name = 'users';

const usersSlice = createSlice({
	name,
	initialState,
	reducers: {
		registerUserRequest(state) {
			state.registerLoading = true;
		},
		registerUserSuccess(state, {payload: userData}) {
			state.registerLoading = false;
			state.registerError = null;
			state.user = userData;
		},
		registerUserFailure(state, action) {
			state.registerLoading = false;
			state.registerError = action.payload;
		},
		logoutUserRequest(state) {
			state.user = null;
		},
		loginUserRequest(state) {
			state.loginLoading = true
		},
		loginUserSuccess(state, action) {
			state.loginLoading = false;
			state.loginError = null;
			state.user = action.payload;
		},
		loginUserFailure(state, action) {
			state.loginLoading = false;
			state.loginError = action.payload;
		},
		clearErrorUser(state) {
			state.loginError = null;
			state.registerError = null;
		},
	},
});

export default usersSlice;