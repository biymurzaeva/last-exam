import React, {useEffect, useState} from 'react';
import {Link, Link as RouterLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import LockOpenOutlinedIcon from '@mui/icons-material/LockOpenOutlined';
import {Avatar, Container, Grid, TextField, Typography} from "@mui/material";
import {clearErrorUser, loginUserRequest} from "../../store/actions/userActions";
import {Alert} from "@mui/lab";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {makeStyles} from "@mui/styles";

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  alert: {
    marginTop: theme.spacing(3),
    width: "100%",
  },
}));

const Login = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const error = useSelector(state => state.users.loginError);
  const loading = useSelector(state => state.users.loginLoading);

  const [user, setUser] = useState({
	  email: '',
    password: ''
  });

	useEffect(() => {
		return () => {
			dispatch(clearErrorUser());
		};
	}, [dispatch]);

  const inputChangeHandler = e => {
    const {name, value} = e.target;

    setUser(prevState => ({...prevState, [name]: value}));
  };

  const submitFormHandler = e => {
    e.preventDefault();
    dispatch(loginUserRequest({...user}));
  };

  return (
    <Container component="section" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOpenOutlinedIcon/>
        </Avatar>
        <Typography component="h1" variant="h6">
          Sign in
        </Typography>
        {
          error &&
          <Alert severity="error" className={classes.alert}>
              {error.message || error.global}
          </Alert>
        }
        <Grid
          component="form"
          container
          className={classes.form}
          onSubmit={submitFormHandler}
          spacing={2}
          noValidate
        >
	        <Grid item xs={12}>
		        <TextField
			        type="email"
			        fullWidth
			        required
			        autoComplete="new-email"
			        label="Email"
			        name="email"
			        value={user.email}
			        onChange={inputChangeHandler}
		        />
	        </Grid>
	        <Grid item xs={12}>
		        <TextField
			        type="password"
			        fullWidth
			        required
			        autoComplete="new-password"
			        label="Password"
			        name="password"
			        value={user.password}
			        onChange={inputChangeHandler}
		        />
	        </Grid>
          <Grid item xs={12}>
            <ButtonWithProgress
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              loading={loading}
              disabled={loading}
            >
              Sign in
            </ButtonWithProgress>
          </Grid>
          <Grid item container justifyContent="flex-end">
            <Link component={RouterLink} variant="body2" to="/register">
              Or sign up
            </Link>
          </Grid>
        </Grid>
      </div>
    </Container>
  );
};

export default Login;