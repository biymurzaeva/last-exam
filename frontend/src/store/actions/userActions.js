import usersSlice from "../slices/userSlice";

export const {
	registerUserRequest,
	registerUserSuccess,
	registerUserFailure,
	loginUserRequest,
	loginUserSuccess,
	loginUserFailure,
	logoutUserRequest,
	clearErrorUser,
} = usersSlice.actions;