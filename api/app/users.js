const express = require("express");
const path = require("path");
const multer = require("multer");
const {nanoid} = require("nanoid");
const config = require("../config");
const User = require("../models/User");

const router = express.Router();

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

router.post('/', upload.single('avatar'), async (req, res) => {
	try {
		const userData = {
			email: req.body.email,
			password: req.body.password,
		};

		if (req.file) userData.avatar = '/uploads/' + req.file.filename;

		const user = new User(userData);

		user.generateToken();
		await user.save();
		res.send(user);
	} catch (error) {
		res.status(400).send(error);
	}
});

router.post('/sessions', async (req, res) => {
	const user = await User.findOne({email: req.body.email});

	if (!user) {
		return res.status(401).send({message: 'Credentials are wrong'});
	}

	const isMatch = await user.checkPassword(req.body.password);

	if (!isMatch) {
		return res.status(401).send({message: 'Credentials are wrong'});
	}

	try {
		user.generateToken();
		await user.save({validateBeforeSave: false});
		res.send({message: 'Email and password correct!', user});
	} catch (e) {
		res.status(400).send(e);
	}
});

router.delete('/sessions', async (req, res) => {
	const token = req.get('Authorization');
	const success = {message: 'Success'};

	if (!token) return res.send(success);

	const user = await User.findOne({token});

	if (!user) return res.send(success);

	user.generateToken();

	await user.save({validateBeforeSave: false});

	return res.send(success);
});

module.exports = router;