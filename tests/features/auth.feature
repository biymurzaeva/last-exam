@registerUser
Feature: Register user
  As a user who want to register
  I should be able to create account
  When I going on registration page
  Then I enter my details data

  Scenario:
    Given I have entered in "/register" page
    When I enter data :
      | email    | user317@gmail.com |
      | password | 123qwerty         |
    And I press form button "Sign up"
    Then I wait 5 sec until I see the text "Registration successful"

@loginUser
  Scenario:
    Given I have entered in "/login" page
    When I enter data :
      | email    | user@gmail.com |
      | password | user           |
    And I press form button "Sign in"
    Then I wait 5 sec until I see the text "Login successful!"
