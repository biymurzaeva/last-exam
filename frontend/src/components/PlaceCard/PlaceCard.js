import React from 'react';
import {makeStyles} from "@mui/styles";
import {apiURL} from "../../config";
import {Card, CardActions, CardContent, CardMedia, Grid, Typography} from "@mui/material";
import {Link} from "react-router-dom";

const useStyles = makeStyles(() => ({
	card: {
		height: '100%',
		display: 'flex',
		flexDirection: 'column',
	},
	cardMedia: {
		paddingTop: '65%',
	},
	cardContent: {
		flexGrow: 1,
	},
	cardAction: {
		display: "flex"
	},
}));

const PlaceCard = props => {
	const classes = useStyles();

	return (
		<Grid item xs={12} sm={6} md={4}>
			<Card className={classes.card}>
				<CardMedia
					className={classes.cardMedia}
					image={apiURL + '/' + props.image}
					title="Image title"
				/>
				<CardContent className={classes.cardContent}>
					<Typography gutterBottom variant="h5" component={Link} to={`/places?place=${props.id}`}>
						{props.title}
					</Typography>
				</CardContent>
				<CardActions className={classes.cardAction}>

				</CardActions>
			</Card>
		</Grid>
	);
};

export default PlaceCard;