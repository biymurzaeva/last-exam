import React, {useState} from 'react';
import {Grid, MenuItem, TextField} from "@mui/material";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";
import {useDispatch, useSelector} from "react-redux";
import {useLocation} from "react-router-dom";
import {addReview} from "../../store/actions/reviewsActions";

const ReviewForm = () => {
	const dispatch = useDispatch();
	const location = useLocation();
	const loading = useSelector(state => state.reviews.addReviewLoading);

	const numbers = [{id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}];

	const [state, setState] = useState({
		review: '',
		ratingFood: '',
		ratingInterior: '',
		ratingService: '',
	});

	const inputChangeHandler = e => {
		const {name, value} = e.target;
		setState(prevState => ({...prevState, [name]: value}));
	};

	const submitFormHandler = e => {
		e.preventDefault();
		dispatch(addReview({place: location.search, review: state}));
	};

	return (
		<Grid
			component="form"
			container
			onSubmit={submitFormHandler}
			noValidate
		>
			<Grid item xs={6}>
				<TextField
					fullWidth
					multiline
					rows={4}
					label="Review"
					name="review"
					onChange={inputChangeHandler}
					value={state.description}
				/>
			</Grid>
			<Grid container spacing={2} mt={2} mb={2}>
				<Grid item xs={2}>
					<TextField
						fullWidth
						select
						variant="outlined"
						label="Quality of food"
						name="ratingFood"
						value={state.ratingFood}
						onChange={inputChangeHandler}
					>
						{numbers.map((number, i) => (
							<MenuItem	key={i}	value={number.id}>
								{number.id}
							</MenuItem>
						))}
					</TextField>
				</Grid>
				<Grid item xs={2}>
					<TextField
						fullWidth
						select
						variant="outlined"
						label="Service quality"
						name="ratingService"
						value={state.ratingService}
						onChange={inputChangeHandler}
					>
						{numbers.map((number, i) => (
							<MenuItem key={i} value={number.id}>
								{number.id}
							</MenuItem>
						))}
					</TextField>
				</Grid>
				<Grid item xs={2}>
					<TextField
						fullWidth
						select
						variant="outlined"
						label="Interior"
						name="ratingInterior"
						value={state.ratingInterior}
						onChange={inputChangeHandler}
					>
						{numbers.map((number, i) => (
							<MenuItem key={i} value={number.id}>
								{number.id}
							</MenuItem>
						))}
					</TextField>
				</Grid>
			</Grid>
			<Grid item>
				<ButtonWithProgress
					type="submit"
					variant="contained"
					color="primary"
					loading={loading}
					disabled={loading}
				>
					Submit review
				</ButtonWithProgress>
			</Grid>
		</Grid>
	);
};

export default ReviewForm;