import React, {useState} from 'react';
import {Checkbox, Container, FormControlLabel, Grid, TextField, Typography} from "@mui/material";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";

const PlaceForm = ({onSubmit, error, loading}) => {
	const [state, setState] = useState({
		title: '',
		description: '',
		image: '',
		agree: false,
	});

	const inputChangeHandler = e => {
		const {name, value} = e.target;
		setState(prevState => ({...prevState, [name]: value}));
	};

	const fileChangeHandler = e => {
		const name = e.target.name;
		const file = e.target.files[0];
		setState(prevState => {
			return {...prevState, [name]: file};
		});
	};

	const getFieldError = fieldName => {
		try {
			return error.errors[fieldName].message;
		} catch (e) {
			return undefined;
		}
	};

	const submitFormHandler = e => {
		e.preventDefault();

		const formData = new FormData();

		Object.keys(state).forEach(key => {
			formData.append(key, state[key]);
		});

		onSubmit(formData);
	};

	return (
		<Container component="section" maxWidth="sm">
			<Grid
				component="form"
				container
				onSubmit={submitFormHandler}
				spacing={2}
				noValidate
			>
				<Grid item xs={12}>
					<TextField
						fullWidth
						type="text"
						required
						label="Title"
						name="title"
						value={state.title}
						onChange={inputChangeHandler}
						error={Boolean(getFieldError('title'))}
						helperText={getFieldError('title')}
					/>
				</Grid>
				<Grid item xs={12}>
					<TextField
						fullWidth
						multiline
						rows={4}
						label="Description"
						name="description"
						onChange={inputChangeHandler}
						value={state.description}
						error={Boolean(getFieldError('description'))}
						helperText={getFieldError('description')}
					/>
				</Grid>
				<Grid item xs={12}>
					<TextField
						fullWidth
						type="file"
						name="image"
						required
						onChange={fileChangeHandler}
						error={Boolean(getFieldError('mainPhoto'))}
						helperText={getFieldError('mainPhoto')}
					/>
				</Grid>
				<Grid item xs={12}>
					<p>
						By submitting this form, you agree that the following information will be submitted to the public
						domain, and administrators of this site will have full control over the said information
					</p>
					<FormControlLabel
						control={
							<Checkbox
								onClick={() => {
									setState(prevState => ({...prevState, agree: !prevState.agree}));
								}}
								checked={state.agree}
								color="success"
							/>
						}
						label={<Typography variant={"subtitle2"}>I understand</Typography>}
						labelPlacement='end'
					/>
				</Grid>
				<Grid item xs={12}>
					<ButtonWithProgress
						type="submit"
						fullWidth
						variant="contained"
						color="primary"
						loading={loading}
						disabled={loading}
					>
						Create
					</ButtonWithProgress>
				</Grid>
			</Grid>
		</Container>
	);
};

export default PlaceForm;