import React from 'react';
import {Typography} from "@mui/material";

const NotFoundPage = () => {
	return (
		<div>
			<Typography component="h1" variant="h5">Not found</Typography>
		</div>
	);
};

export default NotFoundPage;