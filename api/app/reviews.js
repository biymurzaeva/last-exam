const express = require('express');
const auth = require("../middleware/auth");
const Review = require("../models/Review");
const Place = require("../models/Place");

const router = express.Router();

router.get('/', async (req, res) => {
	try {
		if (req.query.place) {
			const reviews = await Review.find({place: req.query.place}).populate('user');
			return res.send(reviews);
		}

		const reviews = await Review.find();
		res.send(reviews);
	} catch (error) {
		res.status(500).send(error);
	}
});

router.post('/', auth, async (req, res) => {
	try {
		if (req.query.place) {
			const newReview = {
				user: req.user._id,
				date: new Date().toISOString(),
				place: req.query.place,
				review: req.body.review,
				ratingFood: req.body.ratingFood || 0,
				ratingService: req.body.ratingService || 0,
				ratingInterior: req.body.ratingInterior || 0,
			};

			const place = await Review.find({place: req.query.place});

			const hasUserReview = await Review.findOne({user: req.user._id, place: req.query.place});

			const sumFood = place.reduce((acc, b) => acc += b['ratingFood'], 0);
			const sumService = place.reduce((acc, b) => acc += b['ratingService'], 0);
			const sumInterior = place.reduce((acc, b) => acc += b['ratingInterior'], 0);

			const votes = place.length;

			const ratingFood = (sumFood/votes);
			const ratingService = (sumService/votes);
			const ratingInterior = (sumInterior/votes);

			if (hasUserReview) {
				await Review.findOneAndUpdate(
					{user: req.user._id, place: req.query.place},
					newReview,
					{new: true, runValidators: true}
				);

				const placeData = await Place.findByIdAndUpdate(
					req.query.place,
					{ratingFood, ratingService, ratingInterior},
					{new: true}
				);

				return res.send(placeData);

			} else {
				const placeData = await Place.findByIdAndUpdate(
					req.query.place,
					{ratingFood, ratingService, ratingInterior},
					{new: true}
				);

				const newUserReview = new Review(newReview);
				await newUserReview.save();

				return res.send(placeData);
			}
		}
	} catch (error) {
		res.status(500).send(error);
	}
});

module.exports = router;