const mongoose = require('mongoose');

const ReviewSchema = new mongoose.Schema({
	place: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Place',
		required: true,
	},
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true,
	},
	date: {
		type: String,
		required: true,
	},
	review: String,
	ratingFood: {
		type: Number,
		min: 0,
		max: 5,
	},
	ratingInterior: {
		type: Number,
		min: 0,
		max: 5,
	},
	ratingService: {
		type: Number,
		min: 0,
		max: 5,
	},
});

const Review = mongoose.model('Review', ReviewSchema);
module.exports = Review;