const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const exitHook = require('async-exit-hook');
const config = require('./config');
const users = require('./app/users');
const places = require('./app/places');
const reviews = require('./app/reviews');
const images = require('./app/images');

const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'));

app.use('/users', users);
app.use('/places', places);
app.use('/reviews', reviews);
app.use('/images', images);

const run = async () => {
	await mongoose.connect(config.db.url);

	app.listen(config.port, () => {
		console.log(`Server started on ${config.port} port!`);
	});

	exitHook(() => {
		console.log('exiting');
		mongoose.disconnect();
	});
};

run().catch(e => console.log(e));