import React from 'react';
import {Link} from "react-router-dom";
import {Button, Grid} from "@mui/material";

const AnonymousMenu = () => {
  return (
    <Grid item container justifyContent="flex-end" alignItems="center">
      <Button component={Link} to="/register" color="inherit">Sign up</Button>
    </Grid>
  );
};

export default AnonymousMenu;