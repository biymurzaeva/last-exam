import placesSlice from "../slices/placesSlice";

export const {
	createPlace,
	createPlaceSuccess,
	createPlaceFailure,
	fetchPlaces,
	fetchPlacesSuccess,
	fetchPlacesFailure,
	fetchPlace,
	fetchPlaceSuccess,
	fetchPlaceFailure,
} = placesSlice.actions;