const express = require("express");
const path = require("path");
const multer = require("multer");
const {nanoid} = require("nanoid");
const config = require("../config");
const auth = require("../middleware/auth");
const Photo = require("../models/Image");

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
	try {
		if (req.query.place) {
			const images = await Photo.find({place: req.query.place});

			if (!images) {
				return res.status(404).send({error: 'Place photos not found'});
			}

			return res.send(images);
		}
	} catch (error) {
		res.status(500).send(error);
	}
});

router.post('/', auth, upload.single('image'), async (req, res) => {
	try {
		if (req.query.place) {
			const newImageData = {
				user: req.user._id,
				place: req.query.place,
			};

			if (req.file) {
				newImageData.image = '/uploads/' + req.file.filename;
			}

			const data = new Photo(newImageData);
			await data.save();
			return res.send(data);
		}
	} catch (error) {
		res.status(500).send(error);
	}
});

module.exports = router;