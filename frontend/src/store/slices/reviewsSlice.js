import {initialState} from "./placesSlice";
const {createSlice} = require("@reduxjs/toolkit");

const name = 'reviews';

const reviewsSlice = createSlice({
	name,
	initialState: {
		reviews: [],
		fetchReviewsLoading: false,
		fetchReviewsError: null,
		addReviewLoading: false,
		addReviewError: null,
	},
	reducers: {
		fetchReviews(state) {
			state.fetchReviewsLoading = true;
		},
		fetchReviewsSuccess(state, action) {
			state.fetchReviewsLoading = false;
			state.reviews = action.payload;
		},
		fetchReviewsFailure(state, action) {
			state.fetchReviewsLoading = false;
			state.fetchReviewsError = action.payload;
		},
		addReview(state) {
			state.addReviewLoading = true;
		},
		addReviewSuccess(state, action) {
			state.addReviewLoading = false;
			initialState.place = action.payload;
		},
		addReviewFailure(state, action) {
			state.addReviewLoading = false;
			state.addReviewError = action.payload;
		},
	},
});

export default reviewsSlice;