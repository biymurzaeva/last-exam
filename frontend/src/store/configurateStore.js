import axiosApi from "../axiosApi";
import {combineReducers} from "redux";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import createSagaMiddleware from 'redux-saga';
import {configureStore} from "@reduxjs/toolkit";
import {rootSagas} from "./rootSagas";
import userSlice, {initialState} from "./slices/userSlice";
import placesSlice from "./slices/placesSlice";
import reviewsSlice from "./slices/reviewsSlice";
import imagesSlice from "./slices/imagesSlice";

const rootReducer = combineReducers({
	'users': userSlice.reducer,
	'places': placesSlice.reducer,
	'reviews': reviewsSlice.reducer,
	'images': imagesSlice.reducer,
});

const persistedState = loadFromLocalStorage();

const sagaMiddleware = createSagaMiddleware();

const middleware = [
	sagaMiddleware,
];

const store = configureStore({
	reducer: rootReducer,
	middleware,
	devtools: true,
	preloadedState: persistedState,
});

sagaMiddleware.run(rootSagas);

store.subscribe(() => {
	saveToLocalStorage({
		users: {
			...initialState,
			user: store.getState().users.user
		},
	});
});

axiosApi.interceptors.request.use(config => {
	try {
		config.headers['Authorization'] = store.getState().users.user.token
	} catch (e) {}
	return config;
});

axiosApi.interceptors.response.use(res => res, e => {
	if (!e.response) {
		e.response = {data: {global: 'No Internet'}};
	}

	throw e;
});

export default store;