import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {
	fetchImages,
	fetchImagesFailure,
	fetchImagesSuccess,
	uploadImage,
	uploadImageFailure,
	uploadImageSuccess
} from "../actions/imagesActions";

export function* uploadImageSaga({payload: data}) {
	try {
		yield axiosApi.post(`/images${data.id}`, data.data);
		yield put(uploadImageSuccess());
		toast.success('Added new image!');
	} catch (error) {
		yield put(uploadImageFailure(error.response.data));
		if (error.response.data?.error) toast.error(error.response.data.error);
	}
}

export function* fetchImagesSaga({payload: id}) {
	try {
		const response = yield axiosApi.get(`/images${id}`);
		yield put(fetchImagesSuccess(response.data));
	} catch (error) {
		yield put(fetchImagesFailure(error.response.data));
		if (error.response.data?.error) toast.error(error.response.data.error);
	}
}

const imagesSaga = [
	takeEvery(uploadImage, uploadImageSaga),
	takeEvery(fetchImages, fetchImagesSaga),
];

export default imagesSaga;