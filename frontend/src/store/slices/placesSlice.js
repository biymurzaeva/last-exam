const {createSlice} = require("@reduxjs/toolkit");

const name = 'places';

export const initialState = {
	place: {},
	places: [],
	fetchPlacesLoading: false,
	fetchPlacesError: null,
	fetchPlaceLoading: false,
	fetchPlaceError: null,
	createPlaceLoading: false,
	createPlaceError: null,
};

const placesSlice = createSlice({
	name,
	initialState,
	reducers: {
		fetchPlaces(state) {
			state.fetchPlacesLoading = true;
		},
		fetchPlacesSuccess(state, action) {
			state.fetchPlacesLoading = false;
			state.places = action.payload;
		},
		fetchPlacesFailure(state, action) {
			state.fetchPlacesLoading = false;
			state.fetchPlacesError = action.payload;
		},
		fetchPlace(state) {
			state.fetchPlaceLoading = true;
		},
		fetchPlaceSuccess(state, action) {
			state.fetchPlaceLoading = false;
			state.place = action.payload;
		},
		fetchPlaceFailure(state, action) {
			state.fetchPlaceLoading = false;
			state.fetchPlaceError = action.payload;
		},
		createPlace(state) {
			state.createPlaceLoading = true;
		},
		createPlaceSuccess(state) {
			state.createPlaceLoading = false;
		},
		createPlaceFailure(state, action) {
			state.createPlaceLoading = false;
			state.createPlaceError = action.payload;
		},
	},
});

export default placesSlice;