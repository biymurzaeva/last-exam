import {all} from 'redux-saga/effects';
import usersSagas from "./sagas/userSagas";
import historySagas from "./sagas/historySagas";
import placesSagas from "./sagas/placesSagas";
import reviewsSagas from "./sagas/reviewsSagas";
import imagesSagas from "./sagas/imagesSagas";

export function* rootSagas() {
	yield all([
		...usersSagas,
		...historySagas,
		...placesSagas,
		...reviewsSagas,
		...imagesSagas,
	]);
}