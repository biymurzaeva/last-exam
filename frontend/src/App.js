import Layout from "./components/UI/Layout/Layout";
import {Route, Routes} from "react-router-dom";
import NotFoundPage from "./containers/NotFoundPage/NotFoundPage";
import MainPage from "./containers/MainPage/MainPage";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AddPlace from "./containers/AddPlace/AddPlace";
import PlaceItems from "./components/PlaceItems/PlaceItems";

const App = () => {
	return (
		<Layout>
			<Routes>
				<Route path="/" element={<MainPage/>}/>
				<Route path="/register" element={<Register/>}/>
				<Route path="/login" element={<Login/>}/>
				<Route path="/new" element={<AddPlace/>}/>
				<Route path="/places" element={<PlaceItems/>}/>
				<Route path="*" element={<NotFoundPage/>}/>
			</Routes>
		</Layout>
	);
};

export default App;
