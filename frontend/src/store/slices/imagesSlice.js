const {createSlice} = require("@reduxjs/toolkit");

const name = 'images';

const imagesSlice = createSlice({
	name,
	initialState: {
		images: [],
		fetchImagesLoading: false,
		fetchImagesError: null,
		uploadImageLoading: false,
		uploadImageError: null,
	},
	reducers: {
		uploadImage(state) {
			state.uploadImageLoading = true;
		},
		uploadImageSuccess(state) {
			state.uploadImageLoading = false;
			state.uploadImageError = null;
		},
		uploadImageFailure(state,  action) {
			state.uploadImageLoading = false;
			state.uploadImageError = action.payload;
		},
		fetchImages(state) {
			state.fetchImagesLoading = true;
		},
		fetchImagesSuccess(state, action) {
			state.fetchImagesLoading = false;
			state.fetchImagesError = null;
			state.images = action.payload;
		},
		fetchImagesFailure(state,  action) {
			state.fetchImagesLoading = false;
			state.fetchImagesError = action.payload;
		},
	},
});

export default imagesSlice;