const mongoose = require('mongoose');

const ImageSchema = new mongoose.Schema({
	image: {
		type: String,
		required: true,
	},
	place: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Place',
		required: true,
	},
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true,
	},
});

const Photo = mongoose.model('Photo', ImageSchema);
module.exports = Photo;