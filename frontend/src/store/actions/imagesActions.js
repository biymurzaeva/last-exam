import imagesSlice from "../slices/imagesSlice";

export const {
	uploadImage,
	uploadImageSuccess,
	uploadImageFailure,
	fetchImages,
	fetchImagesSuccess,
	fetchImagesFailure,
} = imagesSlice.actions;
