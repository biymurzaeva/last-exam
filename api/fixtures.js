const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Place = require("./models/Place");
const Photo = require("./models/Image");
const Review = require("./models/Review");

const run = async () => {
	await mongoose.connect(config.db.url);

	const collections = await mongoose.connection.db.listCollections().toArray();

	for (const coll of collections) {
		await mongoose.connection.db.dropCollection(coll.name);
	}

	const [admin, user1, user2] = await User.create(
		{ email: 'admin@gmail.com',
			password: 'asf',
			token: nanoid(),
			role: 'admin',
		},
		{ email: 'user@gmail.com',
			password: 'asf',
			token: nanoid(),
			role: 'user',
		}, {
			email: 'user2@gmail.com',
			password: 'asf',
			token: nanoid(),
			role: 'user',
		},
	);

	const [place1, place2, place3] = await Place.create(
		{ user: admin,
			title: 'Coffee Place',
			description: 'Coffee shops come in a variety of forms but, traditionally, they are establishments selling ' +
				'prepared coffee, tea, and other hot beverages. More recently, many coffee shops also compete with restaurants ' +
				'in the quick service category by serving baked goods, sandwiches, salads, and other snack items. ' +
				'In 2020/2021, the total coffee consumption worldwide amounted to approximately 166.63 million 60kg bags of' +
				' coffee. One country in particular that stands out as a consumer of the beverage is the United States.' +
				' In 2019, the revenue of the coffee market in the U.S. exceeded 82 billion U.S. dollars. Although coffee ' +
				'drinkers can be found all across the country, the total coffee per capita consumption in the U.S. varied by ' +
				'region in 2020. The Northeast was the leading region in terms of coffee consumption, drinking roughly 1.97 ' +
				'cups of coffee per capita per day. The South, meanwhile, drank the least amount of coffee that year at around 1.8 cups a day.',
			mainPhoto: 'fixtures/bookCoffee.jpeg',
			ratingFood: 4.6,
			ratingInterior: 3.3,
			ratingService: 4.6,
		},
		{ user: user1,
			title: 'My Coffee Place',
			description: 'Coffee shops come in a variety of forms but, traditionally, they are establishments selling ' +
				'prepared coffee, tea, and other hot beverages. More recently, many coffee shops also compete with restaurants ' +
				'in the quick service category by serving baked goods, sandwiches, salads, and other snack items. ' +
				'In 2020/2021, the total coffee consumption worldwide amounted to approximately 166.63 million 60kg bags of' +
				' coffee. One country in particular that stands out as a consumer of the beverage is the United States.' +
				' In 2019, the revenue of the coffee market in the U.S. exceeded 82 billion U.S. dollars. Although coffee ' +
				'drinkers can be found all across the country, the total coffee per capita consumption in the U.S. varied by ' +
				'region in 2020. The Northeast was the leading region in terms of coffee consumption, drinking roughly 1.97 ' +
				'cups of coffee per capita per day. The South, meanwhile, drank the least amount of coffee that year at around 1.8 cups a day.',
			mainPhoto: 'fixtures/bar.jpg',
			ratingFood: 5,
			ratingInterior: 2.6,
			ratingService: 4,
		}, {
			user: user2,
			title: 'Coffee Light',
			description: 'Coffee shops come in a variety of forms but, traditionally, they are establishments selling ' +
				'prepared coffee, tea, and other hot beverages. More recently, many coffee shops also compete with restaurants ' +
				'in the quick service category by serving baked goods, sandwiches, salads, and other snack items. ' +
				'In 2020/2021, the total coffee consumption worldwide amounted to approximately 166.63 million 60kg bags of' +
				' coffee. One country in particular that stands out as a consumer of the beverage is the United States.' +
				' In 2019, the revenue of the coffee market in the U.S. exceeded 82 billion U.S. dollars. Although coffee ' +
				'drinkers can be found all across the country, the total coffee per capita consumption in the U.S. varied by ' +
				'region in 2020. The Northeast was the leading region in terms of coffee consumption, drinking roughly 1.97 ' +
				'cups of coffee per capita per day. The South, meanwhile, drank the least amount of coffee that year at around 1.8 cups a day.',
			mainPhoto: 'fixtures/light.jpg',
			ratingFood: 3.6,
			ratingInterior: 4.3,
			ratingService: 4,
		},
	);

	await Photo.create(
		{ image: '/fixtures/classik.jpeg',
			place: place1,
			user: admin,
		},
		{ image: '/fixtures/coffee.jpeg',
			place: place1,
			user: user1,
		},
		{ image: '/fixtures/coffeePlace1.jpeg',
			place: place2,
			user: user2,
		},
		{ image: '/fixtures/park.jpg',
			place: place2,
			user: admin,
		},{
			image: '/fixtures/save.jpeg',
			place: place3,
			user: user2,
		},
		{ image: '/fixtures/coffeeNo.jpg',
			place: place3,
			user: user1,
		},
	);

	await Review.create(
		{ place: place1,
			user: user1,
			date: new Date("2022-02-15T19:27:03.055Z").toISOString(),
			review: 'Good place...',
			ratingFood: 5,
			ratingInterior: 3,
			ratingService: 5
		},
		{ place: place2,
			user: user1,
			date: new Date("2022-02-15T19:27:03.055Z").toISOString(),
			review: 'Something... test...',
			ratingFood: 5,
			ratingInterior: 3,
			ratingService: 4
		},
		{ place: place1,
			user: user2,
			date: new Date("2022-02-15T19:27:03.055Z").toISOString(),
			review: 'Something...',
			ratingFood: 5,
			ratingInterior: 4,
			ratingService: 5,
		},
		{ place: place2,
			user: user2,
			date: new Date("2022-02-22T19:27:03.055Z").toISOString(),
			review: 'Something...',
			ratingFood: 5,
			ratingInterior: 3,
			ratingService: 4,
		},
		{ place: place1,
			user: admin,
			date: new Date("2022-02-20T19:27:03.055Z").toISOString(),
			review: 'Something...',
			ratingFood: 4,
			ratingInterior: 3,
			ratingService: 4,
		},
		{ place: place2,
			user: admin,
			date: new Date("2022-02-15T19:27:03.055Z").toISOString(),
			review: 'Something...',
			ratingFood: 5,
			ratingInterior: 2,
			ratingService: 4
		},
		{ place: place3,
			user: user1,
			date: new Date("2022-02-14T19:27:03.055Z").toISOString(),
			review: 'Something...',
			ratingFood: 4,
			ratingInterior: 5,
			ratingService: 4
		},
		{ place: place3,
			user: user2,
			date: new Date("2022-02-16T19:27:03.055Z").toISOString(),
			review: 'Something...',
			ratingFood: 3,
			ratingInterior: 3,
			ratingService: 3
		},
		{ place: place3,
			user: admin,
			date: new Date("2022-02-15T19:27:03.055Z").toISOString(),
			review: 'Something...',
			ratingFood: 4,
			ratingInterior: 5,
			ratingService: 5
		},
	);

	await mongoose.connection.close();
};

run().catch(console.error);