import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {apiURL} from "../../config";
import {CircularProgress, Grid, Typography} from "@mui/material";
import {fetchPlace} from "../../store/actions/placesActions";
import {makeStyles} from "@mui/styles";
import {useLocation} from "react-router-dom";
import ReviewForm from "../ReviewForm/ReviewForm";
import {fetchReviews} from "../../store/actions/reviewsActions";
import ImageForm from "../ImageForm/ImageForm";
import {fetchImages} from "../../store/actions/imagesActions";

const useStyles = makeStyles(theme => ({
	img: {
		maxWidth: '250px',
		height: 'auto'
	},
	photos: {
		width: '150px',
	},
	root: {
		display: 'flex',
		flexDirection: 'column',
		'& > * + *': {
			marginTop: theme.spacing(1),
		},
	},
}));

const PlaceItems = () => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const location = useLocation();
	const place = useSelector(state => state.places.place);
	const loading = useSelector(state => state.places.fetchPlaceLoading);
	const usersReviews = useSelector(state => state.reviews.reviews);
	const images = useSelector(state => state.images.images);

	useEffect(() => {
		dispatch(fetchPlace(location.search));
		dispatch(fetchReviews(location.search));
		dispatch(fetchImages(location.search));
	}, [dispatch, location.search]);

	return loading ?
		<Grid container justifyContent="center" alignItems="center">
			<Grid item>
				<CircularProgress/>
			</Grid>
		</Grid> :
		<Grid container mb={4}>
			<Grid item xs={5}>
				<img src={apiURL + '/' + place.mainPhoto} alt={place.title} className={classes.img}/>
			</Grid>
			<Grid item xs={6}>
				<Typography component="h1" variant="h5">{place.title}</Typography>
				<p>{place.description}</p>
			</Grid>
			<Grid item xs={12}>
				<Typography variant="h6">Gallery</Typography>
			</Grid>
				{images && images.map((img, i) => (
					<Grid item key={i} mr={2}>
						<img src={apiURL + img.image} alt='Photo' className={classes.photos}/>
					</Grid>
				))}
			<Grid item xs={12}>
				<Typography variant="h6">Ratings</Typography>
				<p>Quality of food: {place.ratingFood}</p>
				<p>Service quality: {place.ratingService}</p>
				<p>Interior: {place.ratingInterior}</p>
				<hr/>
			</Grid>
			{usersReviews &&
			<Grid item xs={12}>
				<Typography variant="h6">Reviews</Typography>
				{usersReviews.map(r => (
					<div key={r._id}>
						<p>on {r.date.split('T')[0]} {r.user.email} said:</p>
						<p>{r.review}</p>
						<p>Quality of food: {r.ratingFood}</p>
						<p>Service quality: {r.ratingService}</p>
						<p>Interior: {r.ratingInterior}</p>
						<hr/>
					</div>
				))}
			</Grid>}
			<Typography variant="h6" mb={2}>Add new review</Typography>
			<ReviewForm/>
			<Typography variant="h6" mt={2} mb={2}>Upload new image</Typography>
			<ImageForm/>
		</Grid>
};

export default PlaceItems;