import {takeEvery, put} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {
	loginUserRequest,
	loginUserSuccess,
	loginUserFailure,
	logoutUserRequest,
	registerUserRequest,
	registerUserFailure,
	registerUserSuccess
} from "../actions/userActions";
import {historyPush} from "../actions/historyActions";

export function* registerUserSaga ({payload: userData}) {
	try {
		const response = yield axiosApi.post('/users', userData);
		yield put(registerUserSuccess(response.data));
		yield put(historyPush('/'));
		toast.success('Registration successful');
	} catch (error) {
		if (!error.response) toast.error(error.message);
		toast.error(error.response.data.global);
		yield put(registerUserFailure(error.response.data));
	}
}

export function* logoutUserSaga () {
	try {
		yield axiosApi.delete('/users/sessions');
		yield put(historyPush('/'));
	} catch (error) {
		toast.error('Try again');
	}
}

export function* loginUserSaga({payload: userData}) {
	try {
		const response = yield axiosApi.post('/users/sessions', userData);
		yield put(loginUserSuccess(response.data.user));
		yield put(historyPush('/'));
		toast.success('Login successful!');
	} catch (error) {
		yield put(loginUserFailure(error.response.data));
		toast.error(error.response.data.global);
	}
}

const usersSaga = [
	takeEvery(registerUserRequest, registerUserSaga),
	takeEvery(loginUserRequest, loginUserSaga),
	takeEvery(logoutUserRequest, logoutUserSaga),
];

export default usersSaga;