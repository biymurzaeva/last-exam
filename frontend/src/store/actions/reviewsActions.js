import reviewsSlice from "../slices/reviewsSlice";

export const {
	addReview,
	addReviewSuccess,
	addReviewFailure,
	fetchReviews,
	fetchReviewsSuccess,
	fetchReviewsFailure,
} = reviewsSlice.actions;