import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import PlaceForm from "../../components/PlaceForm/PlaceForm";
import {createPlace} from "../../store/actions/placesActions";
import {Container, Typography} from "@mui/material";

const AddPlace = () => {
	const user = useSelector(state => state.users.user);
	const dispatch = useDispatch();
	const error = useSelector(state => state.places.createPlaceError);
	const loading = useSelector(state => state.places.createPlaceLoading);

	const placeSubmit = placeData => {
		dispatch(createPlace(placeData));
	};

	return user && (
		<Container>
			<Typography component="h1" variant="h5" style={{textAlign: "center", marginBottom: '12px'}}>
				Add new place
			</Typography>
			<PlaceForm
				onSubmit={placeSubmit}
				error={error}
				loading={loading}
			/>
		</Container>
	);
};

export default AddPlace;