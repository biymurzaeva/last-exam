const {I} = inject();

Given('I have entered in {string} page', function (page) {
	I.amOnPage(page);
});

When('I enter data :', (table) => {
	table.rows.forEach(row => {
		I.fillField(row.cells[0].value, row.cells[1].value);
	});
});

When('I press form button {string}', (str) => {
	I.click(str, '//main//form//button[@type="submit"]');
});

Then('I see message {string}', (str) => {
	I.see(str);
});

Then('I wait {int} sec until I see the text {string}', function (sec, msg) {
	I.waitForText(msg, sec);
});