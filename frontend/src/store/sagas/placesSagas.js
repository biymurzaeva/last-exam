import {takeEvery, put} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "../actions/historyActions";
import {
	createPlace,
	createPlaceFailure,
	createPlaceSuccess, fetchPlace, fetchPlaceFailure,
	fetchPlaces, fetchPlacesFailure,
	fetchPlacesSuccess, fetchPlaceSuccess
} from "../actions/placesActions";

export function* fetchPlacesSaga() {
	try {
		const response = yield axiosApi.get('/places');
		yield put(fetchPlacesSuccess(response.data));
	} catch (error) {
		yield put(fetchPlacesFailure(error.response.data));
	}
}

export function* fetchPlaceSaga({payload: id}) {
	try {
		const response = yield axiosApi.get(`/places${id}`);
		yield put(fetchPlaceSuccess(response.data));
	} catch (error) {
		yield put(fetchPlaceFailure(error.response.data));
	}
}

export function* createPlacesSaga({payload: data}) {
	try {
		yield axiosApi.post('/places', data);
		yield put(createPlaceSuccess());
		yield put(historyPush('/'));
		toast.success('Added new place!');
	} catch (error) {
		yield put(createPlaceFailure(error.response.data));
		if (error.response.data?.error) toast.error(error.response.data.error);
	}
}

const placesSaga = [
	takeEvery(fetchPlaces, fetchPlacesSaga),
	takeEvery(fetchPlace, fetchPlaceSaga),
	takeEvery(createPlace, createPlacesSaga),
];

export default placesSaga;