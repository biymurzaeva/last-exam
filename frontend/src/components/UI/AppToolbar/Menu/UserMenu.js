import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {Button, Grid, Menu, MenuItem} from "@mui/material";
import defaultAvatar from "../../../../assets/images/default_avatar.png";
import {apiURL} from "../../../../config";
import {logoutUserRequest} from "../../../../store/actions/userActions";
import {Link} from "react-router-dom";

const UserMenu = ({user}) => {
  const dispatch = useDispatch();
	const [anchorEl, setAnchorEl] = useState(null);

	const handleClick = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		setAnchorEl(null);
	};

  return (
    <Grid container justifyContent="space-between" alignItems="center">
	    <img
		    src={user?.avatar ? apiURL + '/' + user.avatar : defaultAvatar}
	      alt="Avatar"
		    style={{
		    	width: '35px',
			    height: '35px',
			    borderRadius: '50%',
		    }}
		    onClick={handleClick}
	    />
	    {user && <Button color='inherit' component={Link} to={'/new'}>Add new place</Button>}
	    <Menu
		    id="simple-menu"
		    anchorEl={anchorEl}
		    keepMounted
		    open={Boolean(anchorEl)}
		    onClose={handleClose}
	    >
		    <MenuItem  onClick={() => dispatch(logoutUserRequest())}>Logout</MenuItem>
	    </Menu>
    </Grid>
  );
};

export default UserMenu;