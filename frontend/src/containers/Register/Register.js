import React, {useEffect, useState} from 'react';
import {Link, Link as RouterLink} from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import {Avatar, Container, Grid, TextField, Typography} from "@mui/material";
import {makeStyles} from "@mui/styles";
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import {clearErrorUser, registerUserRequest} from "../../store/actions/userActions";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Register = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const error = useSelector(state => state.users.registerError);
  const loading = useSelector(state => state.users.registerLoading);

  const [user, setUser] = useState({
	  email: '',
    password: '',
	  avatar: null,
  });

  useEffect(() => {
  	return () => {
  		dispatch(clearErrorUser());
	  };
  }, [dispatch]);

  const inputChangeHandler = e => {
    const {name, value} = e.target;
    setUser(prevState => ({...prevState, [name]: value}));
  };

	const fileChangeHandler = e => {
		const name = e.target.name;
		const file = e.target.files[0];
		setUser(prevState => {
			return {...prevState, [name]: file};
		});
	};

	const submitFormHandler = e => {
    e.preventDefault();

	  const formData = new FormData();

	  Object.keys(user).forEach(key => {
		  formData.append(key, user[key]);
	  });

	  dispatch(registerUserRequest(formData));
  };

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  };

  return (
    <Container component="section" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon/>
        </Avatar>
        <Typography component="h1" variant="h6">
          Sign up
        </Typography>
        <Grid
          component="form"
          container
          className={classes.form}
          onSubmit={submitFormHandler}
          spacing={2}
          noValidate
        >
	        <Grid item xs={12}>
		        <TextField
			        type="email"
			        fullWidth
			        required
			        autoComplete="new-email"
			        label="Email"
			        name="email"
			        value={user.email}
			        onChange={inputChangeHandler}
			        error={Boolean(getFieldError('email'))}
			        helperText={getFieldError('email')}
		        />
	        </Grid>
	        <Grid item xs={12}>
		        <TextField
			        type="password"
			        fullWidth
			        required
			        autoComplete="new-password"
			        label="Password"
			        name="password"
			        value={user.password}
			        onChange={inputChangeHandler}
			        error={Boolean(getFieldError('password'))}
			        helperText={getFieldError('password')}
		        />
	        </Grid>
	        <Grid item xs>
		        <TextField
			        type="file"
			        fullWidth
			        name="avatar"
			        onChange={fileChangeHandler}
			        error={Boolean(getFieldError('avatar'))}
			        helperText={getFieldError('avatar')}
		        />
	        </Grid>
	        <Grid item xs={12}>
		        <ButtonWithProgress
			        type="submit"
			        fullWidth
			        variant="contained"
			        color="primary"
			        className={classes.submit}
			        loading={loading}
			        disabled={loading}
		        >
			        Sign up
		        </ButtonWithProgress>
	        </Grid>

          <Grid item container justifyContent="flex-end">
            <Link component={RouterLink} variant="body2" to="/login">
              Already have an account? Sign in
            </Link>
          </Grid>
        </Grid>
      </div>
    </Container>
  );
};

export default Register;