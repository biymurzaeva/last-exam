import axiosApi from "../../axiosApi";
import {put, takeEvery} from "redux-saga/effects";
import {toast} from "react-toastify";
import {
	addReview,
	addReviewFailure,
	addReviewSuccess, fetchReviews,
	fetchReviewsFailure,
	fetchReviewsSuccess
} from "../actions/reviewsActions";

export function* addReviewSaga({payload: data}) {
	try {
		const response = yield axiosApi.post(`/reviews${data.place}`, data.review);
		yield put(addReviewSuccess(response.data));
		toast.success('Added review!');
	} catch (error) {
		yield put(addReviewFailure(error?.response?.data));
		if (error.response.data?.error) toast.error(error.response.data.error);
	}
}

export function* fetchReviewsSaga({payload: id}) {
	try {
		const response = yield axiosApi.get(`/reviews${id}`);
		yield put(fetchReviewsSuccess(response.data));
	} catch (error) {
		yield put(fetchReviewsFailure(error.response.data));
		if (error.response.data?.error) toast.error(error.response.data.error);
	}
}

const reviewsSaga = [
	takeEvery(addReview, addReviewSaga),
	takeEvery(fetchReviews, fetchReviewsSaga),
];

export default reviewsSaga;