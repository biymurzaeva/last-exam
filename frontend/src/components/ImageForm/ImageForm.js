import React, {useState} from 'react';
import {Grid, TextField} from "@mui/material";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";
import {useDispatch, useSelector} from "react-redux";
import {uploadImage} from "../../store/actions/imagesActions";
import {useLocation} from "react-router-dom";

const ImageForm = () => {
	const dispatch = useDispatch();
	const location = useLocation();
	const [image, setImage] = useState({
		image: '',
	});
	const loading = useSelector(state => state.images.uploadImageLoading);
	const error = useSelector(state => state.images.uploadImageError);

	const fileChangeHandler = e => {
		const name = e.target.name;
		const file = e.target.files[0];
		setImage(prevState => {
			return {...prevState, [name]: file};
		});
	};

	const getFieldError = fieldName => {
		try {
			return error.errors[fieldName].message;
		} catch (e) {
			return undefined;
		}
	};

	const submitFormHandler = e => {
		e.preventDefault();

		const formData = new FormData();

		Object.keys(image).forEach(key => {
			formData.append(key, image[key]);
		});

		dispatch(uploadImage({data: formData, id: location.search}));
	};

	return (
		<Grid
			component="form"
			container
			onSubmit={submitFormHandler}
			spacing={2}
			noValidate
		>
			<Grid item xs={12}>
				<TextField
					fullWidth
					type="file"
					name="image"
					required
					onChange={fileChangeHandler}
					error={Boolean(getFieldError('image'))}
					helperText={getFieldError('image')}
				/>
			</Grid>
			<Grid item xs={12}>
				<ButtonWithProgress
					type="submit"
					variant="contained"
					color="primary"
					loading={loading}
					disabled={loading}
				>
					Upload
				</ButtonWithProgress>
			</Grid>
		</Grid>
	);
};

export default ImageForm;